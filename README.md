Command-line interface for BitBucket issue (ticket) manipulation.

The interesting part is the interactive mode: It shows you a ticket at a time, and you tell it what to do: reject ('invalid'), resolve, set a version (or do nothing). It's a quick way to sort through a bunch of tickets (it would be tiresome to do it through the web interface, or one-by-one through other CLIs).

TODO document
TODO support more operations

Example:


```
#!text


==============================
0 (553): Ticket Title
Ticket Content
Ticket Content contd...
contd...

What to do [Resolve/Invalid/Next/Version/q]?
n

==============================
1 (554): Some other ticket


What to do [Resolve/Invalid/Next/Version/q]?
i No need to do this

==============================
1 (555): Yet another ticket


What to do [Resolve/Invalid/Next/Version/q]?
v 1.5

```

For the first ticket (0 in our batch, #553 in Bitbucket) we did nothing, 'next'.
For the second (1 / #554) we closed as Invalid with a comment "no need to do this".
For the third (1 / #554 - see note below about why this is 1 and not 2) - we changed the version to 1.5.

Serial numbers:
Suppose you have 200 open tickets. You sorted through 50 and now you want to start again from where you stopped. You can use --skip:

./bb.py <repo> .. --skip 50

This will starts from the 50th ticket. Note that this counts only open tickets. The serial number (the first number in the CLI display) fits the skip count, i.e. if you left a ticket open, if you use the number that ./bb.py displayed for it for the --skip argument, you'll start with that same ticket.