#!/usr/bin/python  

import argparse
import requests
import sys
import json

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('repo', help='Repository identifier (e.g. merutak/bitbucket-tickets-cli)')
parser.add_argument('--ticket_id', type=int, help='Ticket ID')
parser.add_argument('--username')
parser.add_argument('--password')
parser.add_argument('--interactive', action='store_true', default=False)
parser.add_argument('--skip', action='store', type=int, default=0)
parser.add_argument('--setver', dest='version', action='store', help='Version to set')
opgroup = parser.add_mutually_exclusive_group(required=False)
opgroup.add_argument('--resolve', action='store_true', default=False, help='Mark ticket as Resolved')
opgroup.add_argument('--invalid', action='store_true', default=False, help='Mark ticket as Invalid')

args = parser.parse_args()

session = requests.Session()
if args.username:
    session.auth = (args.username, args.password)

if args.interactive:
    cmd = ''
    ticketlist = []
    last = args.skip
    while cmd != 'q':
        if not ticketlist:
            resp = session.get('https://bitbucket.org/api/1.0/repositories/%s/issues?start=%s&sort=%s&status=open&status=new'%(args.repo, last, 'utc_created_on', ))
            assert 200 == resp.status_code
            resp = json.loads(resp.content)
            ticketlist = resp['issues']
        if not ticketlist:
            print 'Done'
            sys.exit(0)
        t = ticketlist.pop(0)
        print '%s\n%s (%s): %s\n%s'%('='*30, last, t['local_id'], t['title'], t['content'], )
        last += 1
        url = 'https://bitbucket.org/api/1.0/repositories/%s/issues/%s'%(args.repo, t['local_id'], )
        cmd = ''
        while not cmd:
            print '\nWhat to do [Resolve/Invalid/Next/Version/q]?'
            cmd = sys.stdin.readline().strip()
            if not cmd:
                continue
            if ' ' in cmd:
                cmd, rest = cmd.split(' ', 1)
            else:
                rest = None
            if cmd in ('n', 'next'):
                break
            if cmd in ('q', 'quit'):
                sys.exit(0)

            did_disappear = False
            if cmd in ('r', 'resolve'):
                data = {'status': 'resolved'}
                did_disappear = True
            elif cmd in ('i', 'invalid'):
                data = {'status': 'invalid'}
                did_disappear = True
            elif cmd in ('v', 'version'):
                assert rest
                data = {'version': rest}
                rest = None
            resp = session.put(url, data=data)
            if resp.status_code != 200:
                print 'Status %s: %s'%(resp.status_code, resp.content, )
                cmd = ''
                continue
            if rest is not None:
                resp = session.post(url + '/comments',
                                    data={'content': rest, })
                if resp.status_code == 200:
                    print 'OK'
                else:
                    print 'Failed to post comment:\n%s: %s'%(resp.status_code, resp.content, )
            else:
                print 'OK'
            if did_disappear:
                last -= 1
    sys.exit(0)

def data_for_ops():
    ret = {}
    if args.version is not None:
        ret['version'] = args.version
    if args.resolve:
        ret['status'] = 'resolved'
    if args.invalid:
        ret['status'] = 'invalid' 
    return ret

datastr = data_for_ops()
assert datastr, 'No operations specified!'

url = 'https://bitbucket.org/api/1.0/repositories/%s/issues/%s'%(args.repo, args.ticket_id, )
resp = requests.put(
    url,
    auth=(args.username, args.password) if args.username else None,
    data=datastr)

if resp.status_code == 200:
    print resp.content
    sys.exit(0)
print >> sys.stderr, 'URL: %s\nData: %s\nError %s: %s'%(url, datastr, resp.status_code, resp.content, )
sys.exit(65)
